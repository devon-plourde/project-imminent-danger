﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looping_Object_Spawner : MonoBehaviour {

    public Transform ghost;
    //public BoxCollider2D frame;
    [HideInInspector] public List<Transform> ghostObjects = new List<Transform>();
    [HideInInspector] public bool forceSpawn = false;
    [HideInInspector] public bool update = false;

    bool hasSpawned = false;
    SpriteRenderer sprite;
    Camera cam;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        cam = Camera.main;
    }

    // Use this for initialization
    void FixedUpdate ()
    {
        if(!hasSpawned && (CheckInScreen() || forceSpawn))
            SpawnObjects();
    }

    private void Update()
    {
        if (update)
        {
            int i = 0;
            foreach (var ghost in ghostObjects)
            {
                i++;
                switch (i)
                {
                    case 1:
                        ghost.transform.position = new Vector2(transform.position.x + cam.orthographicSize * 2, transform.position.y);
                        break;
                    case 2:
                        ghost.transform.position = new Vector2(transform.position.x - cam.orthographicSize * 2, transform.position.y);
                        break;
                    case 3:
                        ghost.transform.position = new Vector2(transform.position.x, transform.position.y + cam.orthographicSize * 2);
                        break;
                    case 4:
                        ghost.transform.position = new Vector2(transform.position.x, transform.position.y - cam.orthographicSize * 2);
                        break;
                    case 5:
                        ghost.transform.position = new Vector2(transform.position.x + cam.orthographicSize * 2, transform.position.y + cam.orthographicSize * 2);
                        break;
                    case 6:
                        ghost.transform.position = new Vector2(transform.position.x - cam.orthographicSize * 2, transform.position.y + cam.orthographicSize * 2);
                        break;
                    case 7:
                        ghost.transform.position = new Vector2(transform.position.x + cam.orthographicSize * 2, transform.position.y - cam.orthographicSize * 2);
                        break;
                    case 8:
                        ghost.transform.position = new Vector2(transform.position.x - cam.orthographicSize * 2, transform.position.y - cam.orthographicSize * 2);
                        break;
                    default:
                        break;
                }
                ghost.transform.rotation = transform.rotation;

                //Copying relevant states of physics.
                var ghostPhysics = ghost.GetComponent<Rigidbody2D>();
                var physics = GetComponent<Rigidbody2D>();
                if (ghostPhysics != null)
                {
                    ghostPhysics.velocity = physics.velocity;
                    ghostPhysics.angularVelocity = physics.angularVelocity;
                }

                //If its a ship, copy relevant values
                var ship = GetComponent<Ship_Behaviour>();
                if (ship != null)
                {
                    ghost.transform.Find("Ship Thrust").gameObject.SetActive(ship.Thrust.activeSelf);
                }

                //If its an asteroid, copy relevant values
                var asteroid = GetComponent<Asteroid_Behaviour>();
                if (asteroid != null)
                {
                    //Do stuff
                }

            }
            update = false;
        }
    }

    void SpawnObjects()
    {
        hasSpawned = true;

        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x + cam.orthographicSize*2, transform.position.y), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x - cam.orthographicSize*2, transform.position.y), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x, transform.position.y + cam.orthographicSize*2), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x, transform.position.y - cam.orthographicSize*2), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x + cam.orthographicSize*2, transform.position.y + cam.orthographicSize*2), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x - cam.orthographicSize*2, transform.position.y + cam.orthographicSize*2), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x + cam.orthographicSize*2, transform.position.y - cam.orthographicSize*2), Quaternion.identity));
        ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x - cam.orthographicSize*2, transform.position.y - cam.orthographicSize*2), Quaternion.identity));

        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x + frame.size.x, transform.position.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x - frame.size.x, transform.position.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x, transform.position.y + frame.size.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x, transform.position.y - frame.size.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x + frame.size.x, transform.position.y + frame.size.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x - frame.size.x, transform.position.y + frame.size.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x + frame.size.x, transform.position.y - frame.size.y), Quaternion.identity));
        //ghostObjects.Add(Instantiate(ghost, new Vector2(transform.position.x - frame.size.x, transform.position.y - frame.size.y), Quaternion.identity));

        foreach (var item in ghostObjects)
        {
            var loopingObject = item.GetComponent<Looping_Object>();
            //loopingObject.frame = frame;
            loopingObject.isMainObject = false;
            loopingObject.mainObject = transform;
        }

        update = true;
    }

    bool CheckInScreen()
    {
        if (transform.position.x + sprite.size.x /2 > cam.orthographicSize ||
            transform.position.x - sprite.size.x /2 < -cam.orthographicSize ||
            transform.position.y + sprite.size.y /2 > cam.orthographicSize ||
            transform.position.y - sprite.size.y /2 < -cam.orthographicSize)
            return false;

        return true;
    }
}
