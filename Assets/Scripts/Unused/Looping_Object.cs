﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looping_Object : MonoBehaviour {

    //public BoxCollider2D frame;
    [HideInInspector] public bool isMainObject = true;
    [HideInInspector] public Transform mainObject;

    Camera cam;
    SpriteRenderer sprite;

    private void Start()
    {
        cam = Camera.main;
        sprite = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        moveObjects();
        swapMainObject();
    }

    //Moves objects too far to one side of the screen back to the other side.
    private void moveObjects()
    {
        if (transform.position.x < -cam.orthographicSize * 3)
        {
            transform.position = new Vector2(transform.position.x + cam.orthographicSize * 6, transform.position.y);
        }
        else if (transform.position.x > cam.orthographicSize * 3)
        {
            transform.position = new Vector2(transform.position.x - cam.orthographicSize * 6, transform.position.y);
        }

        if (transform.position.y < -cam.orthographicSize * 3)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + cam.orthographicSize * 6);
        }
        else if (transform.position.y > cam.orthographicSize * 3)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y - cam.orthographicSize * 6);
        }
    }

    private void swapMainObject()
    {
        if (!isMainObject && CheckInScreen())
        {
            var pos = transform.position;
            transform.position = mainObject.position;
            mainObject.position = pos;
        }
    }

    bool CheckInScreen()
    {
        if (transform.position.x + sprite.size.x / 2 > cam.orthographicSize ||
            transform.position.x - sprite.size.x / 2 < -cam.orthographicSize ||
            transform.position.y + sprite.size.y / 2 > cam.orthographicSize ||
            transform.position.y - sprite.size.y / 2 < -cam.orthographicSize)
            return false;

        return true;
    }
}
