﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Solo_Looping : MonoBehaviour {

    //Note: This script needs to figure out what the x and y thresholds are based
    //on the object's rotation. I.E. when the object is rotated 90/270 degrees, the 
    //x threshold will be the y size and vice versa. Any in-between will need to be dealt
    //with using trig?

    SpriteRenderer playArea;
    SpriteRenderer sprite;
    readonly float buffer = 0.1f;
    float playAreaSize;

    private void Start()
    {
        playArea = transform.parent.GetComponent<SpriteRenderer>();
        sprite = GetComponent<SpriteRenderer>();

        //The play area must be square so take either dimension.
        playAreaSize = playArea.size.x > playArea.size.y ? playArea.size.x : playArea.size.y;
        playAreaSize /= 2;
    }

    private void FixedUpdate()
    {
        var temp = playArea.size;

        Vector2 x_form = new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f))
                                        * ((sprite.size.x / 2) * transform.localScale.x),
                                    -Mathf.Sin(transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f))
                                        * ((sprite.size.y / 2) * transform.localScale.y) );

        Vector2 y_form = new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f))
                                        * ((sprite.size.y / 2) * transform.localScale.y),
                                    -Mathf.Sin(transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f))
                                        * ((sprite.size.x / 2) * transform.localScale.x) );

        Vector2 adjustedSize = new Vector2(
            //x adjusted with rotation
            Mathf.Sqrt( Mathf.Pow(x_form.x, 2) + Mathf.Pow(x_form.y, 2) ),
            //y adjusted with rotation
            Mathf.Sqrt(Mathf.Pow(y_form.x, 2) + Mathf.Pow(y_form.y, 2))
        );

        Vector2 thresh = new Vector2( playAreaSize + adjustedSize.x + buffer, 
            playAreaSize + adjustedSize.y + buffer);

        if (transform.position.x > thresh.x)
        {
            transform.position = new Vector2(-thresh.x, transform.position.y);
        }

        else if (transform.position.x < -thresh.x)
        {
            transform.position = new Vector2(thresh.x, transform.position.y);
        }

        if (transform.position.y > thresh.y)
        {
            transform.position = new Vector2(transform.position.x, -thresh.y);
        }

        else if (transform.position.y < -thresh.y)
        {
            transform.position = new Vector2(transform.position.x, thresh.y);
        }
    }
}
