﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputController {

    public delegate void KeyEvent();
    public delegate void AxisEvent(float axis);
    public delegate void WeaponKeyEvent(AuxWeapon weapon);
    public static float horizontalAxis = 0;

    public static KeyEvent TurnLeftKeyDown;
    public static AxisEvent TurnLeftHold;
    public static KeyEvent TurnLeftKeyUp;
    static bool turnLeftState;
    public static bool IsTurnLeftPressed
    {
        get { return turnLeftState; }

        set
        {
            if (turnLeftState != value)
            {
                turnLeftState = value;
                if (turnLeftState)
                {
                    if (TurnLeftKeyDown != null)
                        TurnLeftKeyDown();
                }
                else
                {
                    if (TurnLeftKeyUp != null)
                    {
                        TurnLeftKeyUp();
                    }
                    if (!IsTurnRightPressed)
                        horizontalAxis = 0;
                }
            }
            if (turnLeftState && TurnLeftHold != null)
                if (IsTurnRightPressed)
                    TurnLeftHold(0);
                else
                {
                    if (horizontalAxis > -1)
                    {
                        if (horizontalAxis > 0)
                            horizontalAxis = 0;

                        horizontalAxis -= 0.1f;
                    }

                    TurnLeftHold(horizontalAxis);
                }   
        }
    }

    public static KeyEvent TurnRightKeyDown;
    public static AxisEvent TurnRightHold;
    public static KeyEvent TurnRightKeyUp;
    static bool turnRightState;
    public static bool IsTurnRightPressed
    {
        get { return turnRightState; }

        set
        {
            if (turnRightState != value)
            {
                turnRightState = value;
                if (turnRightState)
                {
                    if (TurnRightKeyDown != null)
                        TurnRightKeyDown();
                }
                else
                {
                    if (TurnRightKeyUp != null)
                    {
                        TurnRightKeyUp();
                    }

                    if (!IsTurnLeftPressed)
                        horizontalAxis = 0;
                }
            }
            if (turnRightState && TurnRightHold != null)
                if (IsTurnLeftPressed)
                    TurnRightHold(0);
                else
                {
                    if (horizontalAxis < 1)
                    {
                        if (horizontalAxis < 0)
                            horizontalAxis = 0;

                        horizontalAxis += 0.1f;
                    }

                    TurnRightHold(horizontalAxis);
                }

        }
    }

    public static KeyEvent FireLasersKeyDown;
    public static KeyEvent FireLaserHold;
    public static KeyEvent FireLasersKeyUp;
    static bool fireLasersState;
    public static bool IsFireLasersPressed
    {
        get { return fireLasersState; }

        set
        {
            if(fireLasersState != value)
            {
                fireLasersState = value;
                if (fireLasersState)
                {
                    if(FireLasersKeyDown != null)
                        FireLasersKeyDown();
                }
                else
                {
                    if (FireLasersKeyUp != null)
                        FireLasersKeyUp();
                }
            }
            if (fireLasersState && FireLaserHold != null)
            {
                FireLaserHold();
            }
        }
    }

    public static KeyEvent ThrustKeyDown;
    public static KeyEvent TrustHold;
    public static KeyEvent ThrustKeyUp;
    static bool thrustState;
    public static bool IsThrustPressed
    {
        get { return thrustState; }

        set
        {
            if (thrustState != value)
            {
                thrustState = value;
                if (thrustState)
                {
                    if (ThrustKeyDown != null)
                        ThrustKeyDown();
                }
                else
                {
                    if (ThrustKeyUp != null)
                        ThrustKeyUp();
                }
            }
            if (thrustState && TrustHold != null)
                TrustHold();
        }
    }

    public static KeyEvent FireAuxKeyDown;
    public static KeyEvent FireAuxHold;
    public static KeyEvent FireAuxKeyUp;
    static bool fireAuxState;
    public static bool IsFireAuxPressed
    {
        get { return fireAuxState; }

        set
        {
            if (fireAuxState != value)
            {
                fireAuxState = value;
                if (fireAuxState)
                {
                    if (FireAuxKeyDown != null)
                        FireAuxKeyDown();
                }
                else
                {
                    if (FireAuxKeyUp != null)
                        FireAuxKeyUp();
                }
            }
            if (fireAuxState && FireAuxHold != null)
                FireAuxHold();
        }
    }

    public static KeyEvent SystemsKeyDown;
    public static KeyEvent SystemsHold;
    public static KeyEvent SystemsKeyUp;
    static bool systemsState;
    public static bool IsSystemsPressed
    {
        get { return systemsState; }

        set
        {
            if (systemsState != value)
            {
                systemsState = value;
                if (systemsState)
                {
                    if (SystemsKeyDown != null)
                        SystemsKeyDown();
                }
                else
                {
                    if (SystemsKeyUp != null)
                        SystemsKeyUp();
                }
            }
            if (systemsState && SystemsHold != null)
                SystemsHold();
        }
    }

    public static KeyEvent CycleAuxKeyDown;
    public static KeyEvent CycleAuxHold;
    public static KeyEvent CycleAuxKeyUp;
    static bool cycleAuxState;
    public static bool IsCycleAuxPressed
    {
        get { return cycleAuxState; }

        set
        {
            if (cycleAuxState != value)
            {
                cycleAuxState = value;
                if (cycleAuxState)
                {
                    if (CycleAuxKeyDown != null)
                        CycleAuxKeyDown();
                }
                else
                {
                    if (CycleAuxKeyUp != null)
                        CycleAuxKeyUp();
                }
            }
            if (cycleAuxState && CycleAuxHold != null)
                CycleAuxHold();
        }
    }

    public static WeaponKeyEvent MissileAuxKeyDown;
    static bool missileAuxState;
    public static bool IsMissileAuxPressed
    {
        get { return missileAuxState; }

        set
        {
            if (missileAuxState != value)
            {
                missileAuxState = value;
                if (missileAuxState)
                {
                    if (MissileAuxKeyDown != null)
                        MissileAuxKeyDown(AuxWeapon.Missile);
                }
            }
        }
    }

    public static WeaponKeyEvent MineAuxKeyDown;
    static bool mineAuxState;
    public static bool IsMineAuxPressed
    {
        get { return mineAuxState; }

        set
        {
            if (mineAuxState != value)
            {
                mineAuxState = value;
                if (mineAuxState)
                {
                    if (MineAuxKeyDown != null)
                        MineAuxKeyDown(AuxWeapon.Mine);
                }
            }
        }
    }

    public static WeaponKeyEvent SpecialAuxKeyDown;
    static bool specialAuxState;
    public static bool IsSpecialAuxPressed
    {
        get { return specialAuxState; }

        set
        {
            if (specialAuxState != value)
            {
                specialAuxState = value;
                if (specialAuxState)
                {
                    if (SpecialAuxKeyDown != null)
                        SpecialAuxKeyDown(AuxWeapon.Special);
                }
            }
        }
    }
}
