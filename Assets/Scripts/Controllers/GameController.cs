﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    [Header("Prefabs")]
    //public Transform ship_prefab;
    public Transform largeAsteroid;

    [Header("UI References")]
    public SpriteRenderer playArea;
    float playAreaSize;

    [Header("Asteroid")]
    public int minLargeAsteroids;
    public int minMediumAsteroids;
    public int minSmallAsteroids;
    public float initialWait;
    public float initialTimeout;
    public float frequencyInc;
    public int startingAsteroids;

    [Header("Player")]
    public Ship_Behaviour ship;

    [Header("Planet")]
    public Defense_Point planet;

    float asteroidTimeout;
    int asteroidsAtOnce;

    private void Awake()
    {
        playAreaSize = playArea.size.x > playArea.size.y ? playArea.size.x : playArea.size.y;
        playAreaSize /= 2;
    }

    // Use this for initialization
    void Start () {
        asteroidTimeout = initialTimeout;
        asteroidsAtOnce = startingAsteroids;

        SetupGame();
    }

    private void SetupGame()
    {
        SpawnShip();

        StartCoroutine(CheckEnemies());
        StartCoroutine(CheckPlayer());
    }

    IEnumerator CheckEnemies()
    {
        yield return new WaitForSeconds(initialWait);

        float timeStarted = Time.time;
        float checkTimer = 5f;

        while (true)
        {
            if ((GameObject.FindGameObjectsWithTag("Large Asteroid").Length < minLargeAsteroids &&
                GameObject.FindGameObjectsWithTag("Medium Asteroid").Length < minMediumAsteroids &&
                GameObject.FindGameObjectsWithTag("Small Asteroid").Length < minSmallAsteroids) ||
                Time.time - timeStarted >= asteroidTimeout)
            {
                timeStarted = Time.time;
                for(int i = 0; i < asteroidsAtOnce; ++i)
                {
                    SpawnAsteroid();
                }

                asteroidTimeout -= frequencyInc;
                if(asteroidTimeout < 60)
                {
                    asteroidTimeout = initialTimeout;
                    asteroidsAtOnce++;
                }
            }

            yield return new WaitForSecondsRealtime(checkTimer);
        }
    }

    IEnumerator CheckPlayer()
    {
        while (true)
        {
            if(!ship.gameObject.activeSelf)
            {
                yield return new WaitForSecondsRealtime(ship.playerSpawnTime);
                ship.Lives -= 1;
                if (ship.Lives > 0)
                    SpawnShip();
                else
                {
                    //game over
                    break;
                }
            }
            yield return null;
        }
    }

    private void SpawnShip()
    {
        //Instantiate(ship_prefab, startingPosition, Quaternion.identity, playArea.transform);
        ship.gameObject.SetActive(true);
    }

    public void SpawnAsteroid()
    {
        var asteroid = Instantiate(largeAsteroid, Vector2.zero, Quaternion.identity, playArea.transform);
        asteroid.GetComponent<Asteroid_Behaviour>().playArea = playArea;

        float thresholdAngle = Mathf.Atan(1); //The screen is the same size up and down so the ratio is 1.
        float angle = Random.Range(0f, Mathf.PI / 2); //This is the angle from the center of the screen where the new asteroid will spawn.
        Vector2 quardrant = new Vector2(Random.Range(-1f, 1f) <= 0 ? -1 : 1, Random.Range(-1f, 1f) <= 0 ? -1 : 1); //This determines which of the 4 quadrants to place the asteroid.

        Vector2 asteroidSize = asteroid.GetComponent<SpriteRenderer>().size / 2;
        asteroidSize = new Vector2(asteroidSize.x * transform.localScale.x, asteroidSize.y * transform.localScale.y);

        //Calculate the new position of the asteroid.
        Vector2 position;
        Vector2 vel;
        if (angle < thresholdAngle)
        {
            float a = playAreaSize; //width of the playing area.
            float b = Mathf.Tan(angle) * a;

            position = new Vector2(a + asteroidSize.x, b);
            vel = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        }
        else
        {
            angle = Mathf.PI / 2 - angle;
            float b = playAreaSize; //height of the playing area.
            float a = Mathf.Tan(angle) * b;

            position = new Vector2(a, b + asteroidSize.y);
            vel = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
        }

        asteroid.transform.position = new Vector2(position.x * quardrant.x, position.y * quardrant.y);
        asteroid.GetComponent<Rigidbody2D>().velocity = new Vector2(vel.x * -quardrant.x, vel.y * -quardrant.y);
        //asteroid.transform.position = new Vector2(14, 14);
        //asteroid.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 0);
    }
}
