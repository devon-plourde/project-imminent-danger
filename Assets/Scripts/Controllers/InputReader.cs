﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputReader : MonoBehaviour {

    [Header("Keyboard Controls")]
    public KeyCode TurnLeft;
    public KeyCode TurnRight;
    public KeyCode Thrust;
    public KeyCode FireLasers;
    public KeyCode FireAux;
    public KeyCode CycleAux;
    public KeyCode Systems;

    [Header("UI Controls")]
    public EnhancedButton TurnLeftUI;
    public EnhancedButton TurnRightUI;
    public EnhancedButton ThrustUI;
    public EnhancedButton FireLasersUI;
    public EnhancedButton FireAuxUI;
    public EnhancedButton MissleAux;
    public EnhancedButton MineAux;
    public EnhancedButton SpecialAux;
    public EnhancedButton SystemToggle0;
    public EnhancedButton SystemToggle1;

    void Update()
    {
        InputController.IsTurnLeftPressed = Input.GetKey(TurnLeft) || TurnLeftUI.isPressed;
        InputController.IsTurnRightPressed = Input.GetKey(TurnRight) || TurnRightUI.isPressed;
        InputController.IsThrustPressed = Input.GetKey(Thrust) || ThrustUI.isPressed;
        InputController.IsFireLasersPressed = Input.GetKey(FireLasers) || FireLasersUI.isPressed;
        InputController.IsFireAuxPressed = Input.GetKey(FireAux) || FireAuxUI.isPressed;
        InputController.IsCycleAuxPressed = Input.GetKey(CycleAux);
        InputController.IsMissileAuxPressed = MissleAux.isPressed;
        InputController.IsMineAuxPressed = MineAux.isPressed;
        InputController.IsSpecialAuxPressed = SpecialAux.isPressed;
        InputController.IsSystemsPressed = Input.GetKey(Systems) || SystemToggle0.isPressed || SystemToggle1.isPressed;
    }



}
