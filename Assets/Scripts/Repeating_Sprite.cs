﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repeating_Sprite : MonoBehaviour {

    public Direction dir = Direction.Right;
    public float scrollSpeed = 0;

    Vector2 threshold;
    Vector2 parentSize;
    Vector2 thisSize;

	// Use this for initialization
	void Start () {
        parentSize = new Vector2(transform.parent.GetComponent<SpriteRenderer>().size.x * transform.parent.transform.localScale.x,
            transform.parent.GetComponent<SpriteRenderer>().size.y * transform.parent.transform.localScale.y);

        thisSize = new Vector2(transform.GetComponent<SpriteRenderer>().size.x * transform.parent.transform.localScale.x,
            transform.GetComponent<SpriteRenderer>().size.y * transform.parent.transform.localScale.y);

        threshold = new Vector2(thisSize.x/2 + parentSize.x/2, thisSize.y / 2 + parentSize.y / 2);

        StartCoroutine(Scroll());
    }
	
    IEnumerator Scroll()
    {
        while (true)
        {
            if (dir == Direction.Left)
            {
                if (transform.position.x < threshold.x)
                {
                    transform.Translate(new Vector2(thisSize.x * 2, 0));
                }
                transform.Translate(new Vector2(-1, 0) * scrollSpeed * Time.deltaTime);
            }
            else
            {
                if (transform.position.x > threshold.x)
                {
                    transform.Translate(new Vector2(-thisSize.x * 2, 0));
                }
                transform.Translate(new Vector2(1, 0) * scrollSpeed * Time.deltaTime);
            }

            yield return null;
        }
    }

    public enum Direction
    {
        Up, Down, Left, Right
    }
}
