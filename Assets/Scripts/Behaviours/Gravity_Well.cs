﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity_Well : MonoBehaviour {

    public float gravity = 1;
    SpriteRenderer playArea;
    float playAreaSize;

	// Use this for initialization
	void Start () {
        playArea = transform.parent.GetComponent<SpriteRenderer>();
        playAreaSize = playArea.size.x > playArea.size.y ? playArea.size.x : playArea.size.y;
        playAreaSize /= 2;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        foreach (Collider2D collider in Physics2D.OverlapCircleAll(transform.position, playAreaSize, LayerMask.GetMask("Enemies"))) {
            Vector3 forceDirection = transform.position - collider.transform.position;
            float distance = Mathf.Sqrt(Mathf.Pow(forceDirection.x, 2) + Mathf.Pow(forceDirection.y, 2));
            collider.GetComponent<Rigidbody2D>().AddForce(forceDirection.normalized * (gravity/ (distance/2)) * Time.fixedDeltaTime);
        }
    }
}
