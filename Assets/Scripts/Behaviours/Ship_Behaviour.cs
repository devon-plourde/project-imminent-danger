﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ship_Behaviour : MonoBehaviour {

    readonly float defaultLaserLength = 50;

    [Header("Components")]
    public GameObject Thrust;
    public Transform MissleBay;

    [Header("Prefabs")]
    public Transform Flare;
    public Transform MissilePrefab;
    public Transform MinePrefab;
    public Transform SpecialPrefab; //This might not even be necessary.

    [Header("Settings")]
    public int startingLives;
    public float playerSpawnTime;
    public float invincibilityTime;
    public SystemType defaultSystem;
    public ShipSystem EngineSystems;
    public ShipSystem WeaponSystems;
    public AuxWeapon staringAux;
    public int startingMissiles;
    public int startingMines;
    public int startingSpecial;

    [Header("UI")]
    //public GameController controller;
    public EnhancedButton missileButton;
    public Text missleCount;
    public EnhancedButton mineButton;
    public Text mineCount;
    public EnhancedButton specialButton;
    public Text specialCount;
    public Text lifeCount;
    public EnhancedButton EnginesSwitch;
    public EnhancedButton WeaponsSwitch;

    GameObject targetL;
    GameObject targetR;
    Transform flareL;
    Transform flareR;
    Rigidbody2D rigid;
    ContactFilter2D filter;

    int _missiles;
    int Missiles
    {
        get { return _missiles; }
        set
        {
            _missiles = value;
            missleCount.text = _missiles.ToString();
        }
    }

    int _mines;
    int Mines
    {
        get { return _mines; }
        set
        {
            _mines = value;
            mineCount.text = _mines.ToString();
        }
    }

    int _special;
    int Special
    {
        get { return _special; }
        set
        {
            _special = value;
            specialCount.text = _special.ToString();
        }
    }

    int _numberOfLives;
    public int Lives
    {
        get { return _numberOfLives; }

        set
        {
            _numberOfLives = value;
            UpdateLifeCount();
        }
    }

    AuxWeapon _currentAux;
    public AuxWeapon SelectedAuxWeapon
    {
        get { return _currentAux; }

        private set
        {
            _currentAux = value;
            SelectAuxWeaponUI();
        }
    }

    ShipSystem _currentSystem;
    public ShipSystem CurrentSystem
    {
        get { return _currentSystem; }
        private set
        {
            if (_currentSystem != value)
            {
                _currentSystem = value;
                rigid.drag = CurrentSystem.LinearDrag;
                UpdateSystemsUI();
            }
        }
    }


    private void Start()
    {
        filter = new ContactFilter2D();
        rigid = GetComponent<Rigidbody2D>();

        //Layermask for raycasting
        int layermask1 = 1 << 9;
        int layermask2 = 1 << 10;
        int layermask3 = 1 << 14;
        filter.layerMask = layermask1 | layermask2 | layermask3;
        filter.useLayerMask = true;

        if (defaultSystem == SystemType.Engines)
            CurrentSystem = EngineSystems;
        else
            CurrentSystem = WeaponSystems;

        Lives = startingLives;
        SelectedAuxWeapon = staringAux;

        InitControls();
    }

    void InitControls()
    {
        InputController.TurnLeftHold += Rotate;
        InputController.TurnRightHold += Rotate;
        InputController.FireLaserHold += InitLasers;
        InputController.FireLasersKeyUp += CancelLasers;
        InputController.FireAuxKeyDown += FireAux;
        InputController.CycleAuxKeyDown += CycleAuxWeapon;
        InputController.MissileAuxKeyDown += ChooseAuxWeapon;
        InputController.MineAuxKeyDown += ChooseAuxWeapon;
        InputController.SpecialAuxKeyDown += ChooseAuxWeapon;
        InputController.TrustHold += ThrustOn;
        InputController.ThrustKeyUp += ThrustOff;
        InputController.SystemsKeyDown += ToggleSystem;
    }

    void RemoveControls()
    {
        InputController.TurnLeftHold -= Rotate;
        InputController.TurnRightHold -= Rotate;
        InputController.FireLaserHold -= InitLasers;
        InputController.FireLasersKeyUp -= CancelLasers;
        InputController.FireAuxKeyDown -= FireAux;
        InputController.CycleAuxKeyDown -= CycleAuxWeapon;
        InputController.TrustHold -= ThrustOn;
        InputController.ThrustKeyUp -= ThrustOff;
        InputController.SystemsKeyDown -= ToggleSystem;
    }

    private void OnEnable()
    {
        Missiles = startingMissiles;
        Mines = startingMines;
        Special = startingSpecial;

        StartCoroutine(StartInvincible());
    }

    IEnumerator StartInvincible()
    {
        float flashSpeed = .125f;
        float startTime = Time.time;

        GetComponent<Collider2D>().enabled = false;
        SpriteRenderer shipSprite = GetComponent<SpriteRenderer>();
        SpriteRenderer thrustSprite = Thrust.GetComponent<SpriteRenderer>();
        Color solid = new Color(255, 255, 255, 255);
        Color transparent = new Color(255, 255, 255, 0);
        while (Time.time - startTime < invincibilityTime)
        {
            if (shipSprite.color.a == 255)
            {
                shipSprite.color = transparent;
                thrustSprite.color = transparent;
            }
            else
            {
                shipSprite.color = solid;
                thrustSprite.color = solid;
            }

            yield return new WaitForSecondsRealtime(flashSpeed);
        }

        shipSprite.color = solid;
        thrustSprite.color = solid;
        GetComponent<Collider2D>().enabled = true;
    }

    private void Rotate(float axis)
    {
        transform.Rotate(new Vector3(0, 0, -CurrentSystem.RotationSpeed * axis));
    }

    private void ThrustOff()
    {
        Thrust.SetActive(false);
        rigid.drag = CurrentSystem.LinearDrag;
    }

    private void ThrustOn()
    {
        Thrust.SetActive(true);

        float rad = transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f);
        rigid.AddForce(new Vector2(-Mathf.Sin(rad), Mathf.Cos(rad)) * CurrentSystem.Acceleration);
        rigid.drag = 0;
    }

    private void FireAux()
    {
        switch (SelectedAuxWeapon)
        {
            case AuxWeapon.Missile:
                if(Missiles > 0)
                {
                    Instantiate(MissilePrefab, MissleBay.position, transform.rotation);
                    --Missiles;
                }
                break;
            case AuxWeapon.Mine:
                if (Mines > 0)
                {
                    Debug.Log("Mine is not implemented yet");
                    --Mines;
                }
                break;
            case AuxWeapon.Special:
                if (Special > 0)
                {
                    Debug.Log("Special is not implemented yet");
                    --Special;
                }
                break;
        }
    }

    private void InitLasers()
    {
        var leftLaser = CurrentSystem.LeftLaser;
        leftLaser.gameObject.SetActive(true);
        FireLaser(leftLaser, ref targetL, ref flareL);

        var rightLaser = CurrentSystem.RightLaser;
        rightLaser.gameObject.SetActive(true);
        FireLaser(rightLaser, ref targetR, ref flareR);
    }

    private void CancelLasers()
    {
        var leftLaser = CurrentSystem.LeftLaser;
        leftLaser.gameObject.SetActive(false);
        RemoveTarget(targetL);
        if(flareL != null)
            Destroy(flareL.gameObject);

        var rightLaser = CurrentSystem.RightLaser;
        rightLaser.gameObject.SetActive(false);
        RemoveTarget(targetR);
        if(flareR != null)
            Destroy(flareR.gameObject);
    }

    private void RemoveTarget(GameObject target)
    {
        if (target != null)
        {
            var asteroid = target.GetComponent<Asteroid_Behaviour>();
            if (asteroid != null)
            {
                asteroid.IsHit = false;
            }
        }
    }

    private void FireLaser(SpriteRenderer laser, ref GameObject target, ref Transform flare)
    {
        RaycastHit2D[] results;
        float rad = transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f);

        results = new RaycastHit2D[1];
        Physics2D.Raycast(laser.transform.position, new Vector2(-Mathf.Sin(rad), Mathf.Cos(rad)), filter, results, defaultLaserLength);
        
        if (target != null && (results[0].collider == null || results[0].collider.gameObject != target))
        {
            RemoveTarget(target);
        }
        if (results[0].collider != null)
            target = results[0].collider.gameObject;
        else
            target = null;


        if (results[0].collider != null)
        {
            laser.size = new Vector2(laser.size.x,
                    Mathf.Sqrt(Mathf.Pow(results[0].point.x - (laser.transform.position.x), 2) +
                        Mathf.Pow(results[0].point.y - (laser.transform.position.y), 2)) / transform.localScale.x);

            if (flare == null)
                flare = Instantiate(Flare, results[0].point, Quaternion.identity, this.transform);
            else
                flare.transform.position = results[0].point;
            
            var enemy = results[0].collider.gameObject.GetComponent<Asteroid_Behaviour>();
            if (enemy != null)
            {
                enemy.energy += CurrentSystem.LaserEnergy;
                enemy.IsHit = true;
            }
        }
        else
        {
            if (flare != null)
                Destroy(flare.gameObject);

            laser.size = new Vector2(laser.size.x, defaultLaserLength);
        }
    }

    public void ToggleSystem()
    {
        CancelLasers();

        if (CurrentSystem.Equals(EngineSystems))
            CurrentSystem = WeaponSystems;
        else
            CurrentSystem = EngineSystems;
    }

    public void CycleAuxWeapon()
    {
        switch (SelectedAuxWeapon)
        {
            case AuxWeapon.Missile:
                SelectedAuxWeapon = AuxWeapon.Mine;
                break;
            case AuxWeapon.Mine:
                SelectedAuxWeapon = AuxWeapon.Special;
                break;
            case AuxWeapon.Special:
                SelectedAuxWeapon = AuxWeapon.Missile;
                break;
        }
    }

    public void ChooseAuxWeapon(AuxWeapon choice)
    {
        SelectedAuxWeapon = choice;
    }

    public void SelectAuxWeaponUI()
    {
        missileButton.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        mineButton.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        specialButton.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        switch (SelectedAuxWeapon)
        {
            case AuxWeapon.Missile:
                missileButton.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                break;
            case AuxWeapon.Mine:
                mineButton.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                break;

            case AuxWeapon.Special:
                specialButton.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                break;
        }
    }

    private void UpdateSystemsUI()
    {
        if (CurrentSystem.Equals(EngineSystems))
        {
            EnginesSwitch.gameObject.SetActive(true);
            WeaponsSwitch.gameObject.SetActive(false);
            WeaponsSwitch.isPressed = false;
        }

        else
        {
            EnginesSwitch.gameObject.SetActive(false);
            EnginesSwitch.isPressed = false;
            WeaponsSwitch.gameObject.SetActive(true);
        }
    }

    private void UpdateLifeCount()
    {
        lifeCount.text = Lives.ToString();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemies") ||
            other.gameObject.layer == LayerMask.NameToLayer("Defense Points") ||
            other.gameObject.layer == LayerMask.NameToLayer("Invincible Defense Point"))
        {
            //RemoveControls();
            //Destroy(gameObject);
            gameObject.SetActive(false);
            CancelLasers();
            //UpdateLifeCount();
        }
    }

    [System.Serializable]
    public class ShipSystem
    {
        [SerializeField] public SpriteRenderer LeftLaser;
        [SerializeField] public SpriteRenderer RightLaser;
        [SerializeField] public float LaserEnergy;
        [SerializeField] public float MaxSpeed;
        [SerializeField] public float Acceleration;
        [SerializeField] public float LinearDrag;
        [SerializeField] public float RotationSpeed;
    }
}

public enum SystemType
{
    Engines, Weapons
}

public enum AuxWeapon
{
    Missile,
    Mine,
    Special
}