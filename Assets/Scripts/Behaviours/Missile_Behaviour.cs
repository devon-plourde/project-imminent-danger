﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile_Behaviour : MonoBehaviour {

    public float initialSpeed;
    public float acceleration;
    public float maxSpeed;
    public GameObject Thrust;

    Rigidbody2D rigid;
    Vector2 init;
    Vector2 acc;
    Vector2 max;

    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody2D>();
        Thrust.SetActive(true);

        //The z value for the rotation is much smaller than I would be expecting. 
        float rad = transform.rotation.eulerAngles.z * ((Mathf.PI * 2f) / 360f);
        init = new Vector2(-Mathf.Sin(rad) * initialSpeed, Mathf.Cos(rad) * initialSpeed);
        acc = new Vector2(-Mathf.Sin(rad) * acceleration, Mathf.Cos(rad) * acceleration);
        max = new Vector2(-Mathf.Sin(rad) * maxSpeed, Mathf.Cos(rad) * maxSpeed);

        rigid.velocity = init;
    }

    private void FixedUpdate()
    {
        Vector2 absMax = new Vector2(Mathf.Abs(max.x), Mathf.Abs(max.y));
        Vector2 absVel = new Vector2(Mathf.Abs(rigid.velocity.x), Mathf.Abs(rigid.velocity.y));

        if(absMax == Vector2.zero || (absVel.x < absMax.x && absVel.y < absMax.y))
            rigid.AddForce(acc);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
