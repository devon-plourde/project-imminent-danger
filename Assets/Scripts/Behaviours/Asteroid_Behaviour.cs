﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Asteroid_Behaviour : MonoBehaviour, INotifyPropertyChanged {

    public float energyDispertion;
    public float maxEnergy;
    public float rotationSpeed;
    public float ejectionSpeed;
    [HideInInspector] public float energy = 0;
    [HideInInspector] public SpriteRenderer playArea;

    bool hit = false;
    [HideInInspector] public bool IsHit { 
        get
        {
            return hit;
        }
        set
        {
            if (value != hit)
            {
                hit = value;
                OnPropertyChanged("hit");
            }
        }
    }
    public List<Transform> children;
    public event PropertyChangedEventHandler PropertyChanged;

    Rigidbody2D rigid;
    Vector2 currentVelocity;
    public Transform mask;

	// Use this for initialization
	void Awake ()
    {
        rigid = GetComponent<Rigidbody2D>();

        rigid.angularVelocity = rotationSpeed/rigid.mass;

        IsHit = false;
        PropertyChanged += OnHitChanged;
    }

    private void Update()
    {
        if (energy > 0 && !IsHit)
        {
            energy = energy - energyDispertion < 0 ? 0 : energy - energyDispertion;
        }

        float instability = energy / maxEnergy;
        mask.localScale = new Vector2(instability, instability);
        if (instability >= 1)
        {
            BreakUp();
        }
    }

    private void FixedUpdate()
    {
        currentVelocity = rigid.velocity;
    }

    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null)
            handler(this, e);
    }

    protected void OnPropertyChanged(string property)
    {
        OnPropertyChanged(new PropertyChangedEventArgs(property));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Projectile")
        {
            BreakUp();
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Defense Points"))
        {
            ContactPoint2D[] contacts = new ContactPoint2D[1];
            collision.GetContacts(contacts);
            rigid.velocity = Vector2.Reflect(currentVelocity, contacts[0].normal);
        }
    }

    private void OnHitChanged(object sender, PropertyChangedEventArgs e)
    {
        if(e.PropertyName == "hit")
        {
            if (hit)
            {
                rigid.velocity /= 2;
            }
            else
            {
                rigid.velocity *= 2;
            }
        }
    }

    private void BreakUp()
    {
        if(children.Count > 0)
        {
            var childAsteroids = children.GetEnumerator();
            int i = -1;
            int j = -1;
            while (childAsteroids.MoveNext())
            {
                var origin = transform.position;
                float angle = Random.Range(0f, Mathf.PI / 2);

                var childSprite = childAsteroids.Current.GetComponent<SpriteRenderer>();
                var child = Instantiate(childAsteroids.Current, 
                    new Vector2(transform.position.x + (childSprite.size.x / 2 * j),
                        transform.position.y + (childSprite.size.y / 2 * i)), transform.rotation, playArea.transform);
                Vector2 localVelocity = new Vector2(ejectionSpeed * Mathf.Cos(angle) * i,
                    ejectionSpeed * Mathf.Sin(angle) * j);
                child.GetComponent<Rigidbody2D>().velocity = new Vector2(rigid.velocity.x + localVelocity.x, 
                    rigid.velocity.y + localVelocity.y);

                child.GetComponent<Asteroid_Behaviour>().playArea = playArea;

                if (j == 1)
                {
                    j = -1;
                    i += 2;
                }
                else
                {
                    j += 2;
                }
            }
        }

        Destroy(gameObject);
    }
}


