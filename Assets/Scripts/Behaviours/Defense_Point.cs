﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Defense_Point : MonoBehaviour {

    public GameObject Shield;
    public int maxNumberOfHits;
    public float invincibilityTime;

    //public GameController controller;
    public Text shieldPercent;
    public Slider shieldSlider;


    int numberOfHits;

    private void Start()
    {
        numberOfHits = 0;
        shieldSlider.maxValue = maxNumberOfHits;
        shieldSlider.minValue = 0;
        UpdateShieldUI();
    }

    private void HitShield()
    {
        if(Shield.gameObject.tag != "Invincible")
        {
            numberOfHits++;
            UpdateShieldUI();
            StartCoroutine(InvincibleAfterHit());
        }

        //Other concequenses of hitting the shield.
    }

    public void UpdateShieldUI()
    {
        int hitsLeft = maxNumberOfHits - numberOfHits;
        if (shieldSlider != null && shieldSlider.value > 0)
        {
            shieldSlider.value = hitsLeft;
        }

        if (shieldPercent != null)
        {
            float percent = ((float)hitsLeft / maxNumberOfHits) * 100;
            shieldPercent.text = percent.ToString("N0");
        }
    }

    IEnumerator InvincibleAfterHit()
    {
        SpriteRenderer sprite = Shield.GetComponent<SpriteRenderer>();
        float currentAlpha = sprite.color.a;
        float alpha = ((float)maxNumberOfHits - numberOfHits) / maxNumberOfHits;

        float flashSpeed = .125f;
        float startTime = Time.time;

        Shield.gameObject.tag = "Invincible";
        while (Time.time - startTime < invincibilityTime)
        {
            if (sprite.color.a > 0)
            {
                sprite.color = new Color(1, 1, 1, 0);
            }
            else
            {
                sprite.color = new Color(1, 1, 1, currentAlpha);
            }

            yield return new WaitForSecondsRealtime(flashSpeed);
        }

        if(numberOfHits < maxNumberOfHits)
        {
            sprite.color = new Color(1, 1, 1, alpha);
            Shield.gameObject.tag = "DefensePoint";
        }
        else
        {
            Shield.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemies"))
        {
            if (Shield.activeSelf)
            {
                HitShield();
            }
            else
            {
                //Game Over
                Destroy(gameObject);
            }
        }
    }
}
