﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour {

    Text clockDisplay;

	// Use this for initialization
	void Start () {
        clockDisplay = GetComponent<Text>();
	}

    // Update is called once per frame
    void Update()
    {
        string min = ((int) Time.time / 60).ToString("D2");
        string sec = ((int) Time.time % 60).ToString("D2");
        clockDisplay.text = string.Format("{0}:{1}", min, sec);
    }
}
